import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MockComponents, MockProvider } from 'ng-mocks';
import { DomainUserEditComponent } from './domain-user-edit.component';
import { ApiGeneralService } from '../../../../../../generated-api';
import { ToastService } from '../../../../../_services/toast.service';
import { DomainUserBadgeComponent } from '../../../domain-user-badge/domain-user-badge.component';
import { ToolsModule } from '../../../../tools/tools.module';
import { InfoIconComponent } from '../../../../tools/info-icon/info-icon.component';
import { mockDomainSelector } from '../../../../../_utils/_mocks.spec';

describe('DomainUserEditComponent', () => {

    let component: DomainUserEditComponent;
    let fixture: ComponentFixture<DomainUserEditComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DomainUserEditComponent, MockComponents(DomainUserBadgeComponent, InfoIconComponent)],
            imports: [RouterTestingModule, ReactiveFormsModule, ToolsModule],
            providers: [
                MockProvider(ApiGeneralService),
                MockProvider(ToastService),
                mockDomainSelector(),
            ],
        });
        fixture = TestBed.createComponent(DomainUserEditComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('is created', () => {
        expect(component).toBeTruthy();
    });
});

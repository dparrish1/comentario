---
title: Script tag
description: The `<script>` tag is required to embed comments on a page
weight: 10
tags:
    - configuration
    - comments
    - embedding
    - HTML
seeAlso:
    - comments-tag
---

The `<script>` tag is the first of the two elements required to embed comments onto a page. It provides the entire commenting functionality, including comment layout, content, and styling.

<!--more-->

The other element is the [comments tag](comments-tag).
